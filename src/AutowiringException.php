<?php

declare(strict_types=1);

namespace Them\Container;

use LogicException;
use Psr\Container\ContainerExceptionInterface;
use Throwable;

final class AutowiringException extends LogicException implements ContainerExceptionInterface
{
    public function __construct(
        public readonly string $class,
        public readonly ?string $method = null,
        public readonly ?string $param = null,
        public readonly ?string $id = null,
        ?Throwable $previous = null,
    ) {
        parent::__construct($this->composeMessage(), 0, $previous);
    }

    private function composeMessage(): string
    {
        $message = "Cannot instantiate class \"$this->class\"";

        if ($this->method !== null) {
            $message .= " (Error while calling method \"$this->method\"";

            if ($this->param !== null) {
                $message .= ", unable to resolve param \"$this->id \$$this->param\"";
            }

            $message .= ')';
        }

        return $message;
    }
}
