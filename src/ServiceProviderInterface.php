<?php

declare(strict_types=1);

namespace Them\Container;

interface ServiceProviderInterface
{
    public function register(Container $container): void;
}
