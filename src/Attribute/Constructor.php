<?php

declare(strict_types=1);

namespace Them\Container\Attribute;

use Attribute;
use ReflectionClass;
use Them\Attributes\Attributes;

#[Attribute(Attribute::TARGET_CLASS)]
final readonly class Constructor
{
    /**
     * @param array<non-empty-string, non-empty-string> $params
     */
    public function __construct(
        public array $params = [],
    ) {}

    /**
     * @param ReflectionClass<object> $reflectionClass
     *
     * @return array<non-empty-string, non-empty-string>
     */
    public static function getParams(ReflectionClass $reflectionClass): array
    {
        return Attributes::oneFromReflection(
            $reflectionClass,
            self::class,
        )->params ?? [];
    }
}
