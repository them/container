<?php

declare(strict_types=1);

namespace Them\Container\Attribute;

use Attribute;
use ReflectionClass;
use ReflectionException;
use Them\Attributes\Attributes;

/**
 * @template TSpecific of object
 */
#[Attribute(Attribute::TARGET_CLASS)]
final readonly class Specific
{
    /**
     * @param class-string<TSpecific> $class
     */
    public function __construct(
        private string $class,
    ) {}

    /**
     * @param class-string $class
     *
     * @return ReflectionClass<object>
     *
     * @throws ReflectionException
     */
    public static function reflect(string $class): ReflectionClass
    {
        $reflectionClass = new ReflectionClass($class);

        $specificClass = Attributes::oneFromReflection(
            $reflectionClass,
            self::class,
        )?->class;

        return $specificClass !== null
            ? self::reflect($specificClass)
            : $reflectionClass;
    }
}
