<?php

declare(strict_types=1);

namespace Them\Container\Attribute;

use Attribute;
use ReflectionClass;
use Them\Attributes\Attributes;

#[Attribute(Attribute::TARGET_CLASS | Attribute::IS_REPEATABLE)]
final readonly class Method
{
    /**
     * @param non-empty-string $name The method to call after initialization
     * @param array<non-empty-string, non-empty-string> $params
     */
    public function __construct(
        public string $name,
        public array $params = [],
    ) {}

    /**
     * @param ReflectionClass<object> $reflectionClass
     *
     * @return array<non-empty-string, array<non-empty-string, non-empty-string>>
     */
    public static function getParams(ReflectionClass $reflectionClass): array
    {
        $attributes = Attributes::fromReflection(
            $reflectionClass,
            self::class,
        );

        $params = [];

        foreach ($attributes as $attribute) {
            $params[$attribute->name] = $attribute->params;
        }

        return $params;
    }
}
