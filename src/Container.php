<?php

declare(strict_types=1);

namespace Them\Container;

use JetBrains\PhpStorm\Deprecated;
use Pimple\Container as Pimple;
use Pimple\ServiceProviderInterface as PimpleServiceProvider;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Them\Container\ServiceProviderInterface as ServiceProvider;

final class Container extends Pimple implements ContainerInterface
{
    /**
     * @param array<string, mixed> $values
     */
    public function __construct(array $values = [])
    {
        parent::__construct($values);

        $this
            ->set(self::class, $this)
            ->aka(self::class, ContainerInterface::class);
    }

    public function set(string $id, mixed $value): self
    {
        $this->offsetSet($id, $value);

        return $this;
    }

    /**
     * @template TObj of object
     * @template TVal of string
     *
     * @param class-string<TObj>|TVal $id
     *
     * @return TObj|mixed
     */
    public function get(string $id): mixed
    {
        return $this->offsetGet($id);
    }

    public function has(string $id): bool
    {
        return $this->offsetExists($id);
    }

    public function aka(string $id, string ...$aliases): self
    {
        foreach ($aliases as $alias) {
            $this->set(
                $alias,
                static fn(self $container): mixed =>
                    $container->get($id),
            );
        }

        return $this;
    }

    /** @codeCoverageIgnore  */
    #[Deprecated(
        'Will be removed in release 2.0',
        '%class%->aka(%parameter1%, %parameter0%)',
    )]
    public function alias(string $alias, string $id): self
    {
        return $this->aka($id, $alias);
    }

    public function offsetGet($id): mixed
    {
        if (
            !parent::offsetExists($id)
            && (class_exists($id) || interface_exists($id))
        ) {
            parent::offsetSet($id, new Autowire($id));
        }

        return parent::offsetGet($id);
    }

    /**
     * @template TSp of ServiceProvider
     * @template TPp of PimpleServiceProvider
     * @template TVal
     *
     * @param class-string<TSp>|class-string<TPp>|TSp|TPp $provider
     * @param TVal[] $values
     *
     * @return $this
     *
     * @throws ContainerExceptionInterface
     */
    public function register(
        string|PimpleServiceProvider|ServiceProvider $provider,
        array $values = [],
    ): self {
        if (is_string($provider)) {
            $provider = (new Autowire($provider))($this);
        }

        $provider->register($this);

        foreach ($values as $key => $value) {
            $this[(string) $key] = $value;
        }

        return $this;
    }
}
