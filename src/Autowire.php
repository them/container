<?php

declare(strict_types=1);

namespace Them\Container;

use Error;
use Generator;
use Psr\Container\ContainerExceptionInterface;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use Them\Container\Attribute\Constructor;
use Them\Container\Attribute\Method;
use Them\Container\Attribute\Specific;

/**
 * @internal
 *
 * @template T of object
 */
final readonly class Autowire
{
    /**
     * @param class-string<T> $class
     */
    public function __construct(
        private string $class,
    ) {}

    /**
     * @param Container $container
     *
     * @return T
     *
     * @throws AutowiringException
     */
    public function __invoke(Container $container): object
    {
        try {
            /** @var ReflectionClass<T> $reflectionClass */
            $reflectionClass = Specific::reflect($this->class);
        } catch (ReflectionException $exception) {
            throw new AutowiringException(
                $this->class,
                previous: $exception,
            );
        }

        $realClass = $reflectionClass->getName();
        if ($this->class !== $realClass) {
            /** @var T $object */
            $object = $container->get($realClass);

            return $object;
        }

        $object = $this->createInstance(
            $container,
            $reflectionClass,
        );

        $this->callMethods(
            $container,
            $reflectionClass,
            $object,
        );

        return $object;
    }

    /**
     * @param ReflectionMethod $reflectionMethod
     *
     * @return array<non-empty-string, non-empty-string>
     */
    private static function getMethodParams(
        ReflectionMethod $reflectionMethod,
    ): array {
        $params = [];

        foreach ($reflectionMethod->getParameters() as $reflectionParameter) {
            /** @var non-empty-string $name */
            $name = $reflectionParameter->getName();
            $type = $reflectionParameter->getType();

            /**
             * @var non-empty-string $typeName
             */
            $typeName = $type !== null
                ? (string) $type
                : '*no type given*';

            $params[$name] = $typeName;
        }

        return $params;
    }

    /**
     * @param Container $container
     * @param array<non-empty-string, non-empty-string> $params
     *
     * @return Generator<non-empty-string, mixed>
     */
    private static function resolveParams(
        Container $container,
        array $params,
    ): Generator {
        foreach ($params as $name => $id) {
            try {
                yield $name => $container->get($id);
            } catch (ContainerExceptionInterface $exception) {
                throw new AutowiringException(
                    '',
                    param: $name,
                    id: $id,
                    previous: $exception,
                );
            }
        }
    }

    /**
     * @param Container $container
     * @param ReflectionClass<T> $reflectionClass
     * @param T $object
     *
     * @return void
     */
    private function callMethods(
        Container $container,
        ReflectionClass $reflectionClass,
        object $object,
    ): void {
        foreach (Method::getParams($reflectionClass) as $name => $params) {
            self::callMethod(
                $reflectionClass,
                $name,
                $params,
                $container,
                $object,
            );
        }
    }

    /**
     * @param Container $container
     * @param ReflectionClass<T> $reflectionClass
     *
     * @return T
     *
     * @throws AutowiringException
     */
    private function createInstance(
        Container $container,
        ReflectionClass $reflectionClass,
    ): object {
        $constructor = $reflectionClass->getConstructor();

        try {
            $resolvedParams = self::resolveParams(
                $container,
                array_merge(
                    $constructor !== null
                        ? self::getMethodParams($constructor)
                        : [],
                    Constructor::getParams($reflectionClass),
                ),
            );

            return $reflectionClass->newInstance(...$resolvedParams);
        } catch (Error|ReflectionException $exception) {
            throw new AutowiringException(
                $reflectionClass->getName(),
                '__construct',
                previous: $exception,
            );
        } catch (AutowiringException $exception) {
            throw new AutowiringException(
                $reflectionClass->getName(),
                '__construct',
                $exception->param,
                $exception->id,
                previous: $exception,
            );
        }
    }

    /**
     * @template C of object
     *
     * @param ReflectionClass<C> $reflectionClass
     * @param non-empty-string $name
     * @param array<non-empty-string, non-empty-string> $params
     * @param Container $container
     * @param C $object
     *
     * @return void
     *
     * @throws AutowiringException
     */
    private static function callMethod(
        ReflectionClass $reflectionClass,
        string $name,
        array $params,
        Container $container,
        object $object,
    ): void {
        try {
            $reflectionMethod = $reflectionClass->getMethod($name);

            $methodParams = array_merge(
                self::getMethodParams($reflectionMethod),
                $params,
            );

            $resolvedParams = self::resolveParams($container, $methodParams);
            $reflectionMethod->invoke($object, ...$resolvedParams);
        } catch (ReflectionException $exception) {
            throw new AutowiringException(
                $reflectionClass->getName(),
                $name,
                previous: $exception,
            );
        } catch (AutowiringException $exception) {
            throw new AutowiringException(
                $reflectionClass->getName(),
                $name,
                $exception->param,
                $exception->id,
                previous: $exception->getPrevious(),
            );
        }
    }
}
