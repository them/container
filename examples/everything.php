<?php

declare(strict_types=1);

use Them\Container\Container;

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/Assets/include.php';

/**
 * Create the container and register something
 */
$container = new Container(['random_int' => random_int(0, 100)]);

/**
 * Register a service provider by its class name - so it gets autowired
 */
$container->register(ServiceProvider::class);

/**
 * Let the container autwire the service
 */
$service = $container->get(Service::class);

var_dump($service);