<?php

declare(strict_types=1);

use Them\Container\Attribute\Constructor;
use Them\Container\Attribute\Method;

#[Constructor(['someRandomInt' => 'random_int'])]
#[Method('setDateTime', ['dateTime' => 'current_time'])]
class Service
{
    public ?DateTimeInterface $dateTime = null;

    public function __construct(
        public readonly DependencyInterface $dependency,
        public readonly int $someRandomInt,
    ) {
    }

    public function setDateTime(DateTimeInterface $dateTime): void
    {
        $this->dateTime = $dateTime;
    }
}
