<?php

declare(strict_types=1);

require_once __DIR__ . '/DependencyInterface.php';
require_once __DIR__ . '/Dependency.php';
require_once __DIR__ . '/Service.php';
require_once __DIR__ . '/ServiceProvider.php';
