<?php

declare(strict_types=1);

use Them\Container\Container;
use Them\Container\ServiceProviderInterface;

class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $container): void
    {
        $container['current_time'] = static fn(): DateTimeImmutable =>
            new DateTimeImmutable();
    }
}
