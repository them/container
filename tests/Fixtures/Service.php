<?php

declare(strict_types=1);

namespace Them\Tests\Container\Fixtures;

use Them\Container\Attribute\Constructor;
use Them\Container\Attribute\Method;

#[Constructor(['dependency2' => DependencyInterface::class])]
#[Method('setDependencyThree')]
#[Method('setDependencyFour', ['dependency4' => DependencyInterface::class])]
final class Service implements ServiceInterface
{
    /** @phpstan-ignore-next-line */
    public DependencyInterface $dependency3;

    /** @phpstan-ignore-next-line */
    public object $dependency4;

    public function __construct(
        public readonly DependencyInterface $dependency1,
        public readonly object $dependency2,
    ) {}

    public function setDependencyThree(DependencyInterface $dependency3): void
    {
        $this->dependency3 = $dependency3;
    }

    public function setDependencyFour(object $dependency4): void
    {
        $this->dependency4 = $dependency4;
    }
}
