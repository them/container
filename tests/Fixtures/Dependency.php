<?php

declare(strict_types=1);

namespace Them\Tests\Container\Fixtures;

final readonly class Dependency implements DependencyInterface {}
