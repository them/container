<?php

declare(strict_types=1);

namespace Them\Tests\Container\Fixtures;

use Them\Container\Attribute\Specific;

#[Specific(Dependency::class)]
interface DependencyInterface {}
