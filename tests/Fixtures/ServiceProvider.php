<?php

declare(strict_types=1);

namespace Them\Tests\Container\Fixtures;

use Them\Container\Attribute\Constructor;
use Them\Container\Container;
use Them\Container\ServiceProviderInterface;

#[Constructor(['injectedValue' => 'test_value'])]
final readonly class ServiceProvider implements ServiceProviderInterface
{
    public function __construct(
        private string $injectedValue,
    ) {}

    public function register(Container $container): void
    {
        $container->set('injected_value', $this->injectedValue);
    }
}
