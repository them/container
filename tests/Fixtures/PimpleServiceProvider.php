<?php

declare(strict_types=1);

namespace Them\Tests\Container\Fixtures;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Them\Container\Attribute\Constructor;

#[Constructor(['injectedValue' => 'test_value'])]
final readonly class PimpleServiceProvider implements ServiceProviderInterface
{
    public function __construct(
        private string $injectedValue,
    ) {}

    public function register(Container $pimple): void
    {
        $pimple['injected_value'] = $this->injectedValue;
    }
}
