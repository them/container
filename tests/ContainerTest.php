<?php

declare(strict_types=1);

namespace Them\Tests\Container;

use PHPUnit\Framework\TestCase;
use Pimple\Exception\UnknownIdentifierException;
use Psr\Container\ContainerInterface;
use Them\Container\Attribute\Method;
use Them\Container\AutowiringException;
use Them\Tests\Container\Fixtures\Dependency;
use Them\Tests\Container\Fixtures\DependencyInterface;
use Them\Tests\Container\Fixtures\PimpleServiceProvider;
use Them\Container\Container;
use Them\Tests\Container\Fixtures\Service;
use Them\Tests\Container\Fixtures\ServiceInterface;
use Them\Tests\Container\Fixtures\ServiceProvider;
use Throwable;

final class ContainerTest extends TestCase
{
    public function testPreRegisteredServices(): void
    {
        $subject = new Container();

        self::assertSame(
            $subject,
            $subject->get(Container::class),
        );

        self::assertSame(
            $subject,
            $subject->get(ContainerInterface::class),
        );
    }

    public function testRegisterWithPimpleProviderInstance(): void
    {
        $testValue = microtime();

        $subject = new Container();
        $subject->register(new PimpleServiceProvider($testValue));

        self::assertSame(
            $testValue,
            $subject->get('injected_value'),
        );
    }

    public function testRegisterWithPimpleProviderClass(): void
    {
        $testValue = microtime();

        $subject = new Container(['test_value' => $testValue]);
        $subject->register(PimpleServiceProvider::class);

        self::assertSame(
            $testValue,
            $subject->get('injected_value'),
        );
    }

    public function testRegisterWithProviderInstance(): void
    {
        $testValue = microtime();

        $subject = new Container();
        $subject->register(new ServiceProvider($testValue));

        self::assertSame(
            $testValue,
            $subject->get('injected_value'),
        );
    }

    public function testRegisterWithProviderClass(): void
    {
        $testValue = microtime();

        $subject = new Container(['test_value' => $testValue]);
        $subject->register(ServiceProvider::class);

        self::assertSame(
            $testValue,
            $subject->get('injected_value'),
        );
    }

    public function testAutowiring(): void
    {
        $subject = new Container();

        $service = $subject->get(ServiceInterface::class);
        self::assertInstanceOf(Service::class, $service);

        self::assertTrue($subject->has(DependencyInterface::class));

        $dependency = $subject->get(DependencyInterface::class);
        self::assertInstanceOf(Dependency::class, $dependency);

        self::assertSame(
            $dependency,
            $service->dependency1,
        );

        self::assertSame(
            $dependency,
            $service->dependency2,
        );

        self::assertSame(
            $dependency,
            $service->dependency3,
        );

        self::assertSame(
            $dependency,
            $service->dependency4,
        );
    }

    public function testAutowiringSomethingUnknown(): void
    {
        $this->expectException(UnknownIdentifierException::class);

        (new Container())->get(__FUNCTION__);
    }

    public function testAutowiringSomethingUninstantiatable(): void
    {
        $this->expectException(AutowiringException::class);

        (new Container())->get(Throwable::class);
    }

    public function testAutowiringWithUnknownConstructorParamType(): void
    {
        $this->expectException(AutowiringException::class);

        $class = new class((object) []) {
            /** @phpstan-ignore-next-line */
            public function __construct(object $object) {}
        };

        (new Container())->get($class::class);
    }

    public function testAutowiringWithNoConstructorParamType(): void
    {
        $this->expectException(AutowiringException::class);

        $class = new class((object) []) {
            /** @phpstan-ignore-next-line */
            public function __construct($object) {}
        };

        (new Container())->get($class::class);
    }

    public function testAutowiringWithUnknownMethod(): void
    {
        $this->expectException(AutowiringException::class);

        $class = new #[Method('unknownMethod')] class() {};

        (new Container())->get($class::class);
    }

    public function testAutowiringWithUnknownMethodParam(): void
    {
        $this->expectException(AutowiringException::class);

        $class = new #[Method('set')] class() {
            public function set(object $object): void {}
        };

        (new Container())->get($class::class);
    }

    public function testSingletons(): void
    {
        $container = new Container();

        self::assertSame(
            $container->get(ServiceInterface::class),
            $container->get(Service::class),
        );
    }

    public function testSingletons2(): void
    {
        $container = new Container();

        self::assertSame(
            $container->get(Service::class),
            $container->get(ServiceInterface::class),
        );
    }
}
